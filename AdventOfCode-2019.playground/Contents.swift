import UIKit

// Do Day 1
if let path = Bundle.main.path(forResource: "day1", ofType: "txt") {
    print(SantaSleighFuelRequirements.runSolution(path: path))
}

// Do Day 2
if let path = Bundle.main.path(forResource: "day2", ofType: "txt") {
    print(IntCode.runSolution(path: path))
}

// Do Day 3



