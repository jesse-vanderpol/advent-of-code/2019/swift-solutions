import Foundation

protocol Computer {
    var memory: [Int] { get set }
    mutating func runProgram() -> Int
    mutating func setProgramInputs(noun: Int, verb: Int)
}

enum Operation {
    case add
    case multiply
    case halt
}

protocol Instruction {
    var opcode: Operation { get }
    var pointerInterval: Int { get }
    var param1: Int? { get }
    var param2: Int? { get }
    var param3: Int? { get }
}

public struct AddInstruction: Instruction {
    let opcode: Operation
    let pointerInterval: Int
    let param1: Int?
    let param2: Int?
    let param3: Int?
    
    init(input1: Int, input2: Int, output: Int) {
        self.opcode = .add
        self.pointerInterval = 4
        self.param1 = input1
        self.param2 = input2
        self.param3 = output
    }
}


public struct MultiplyInstruction: Instruction {
    let opcode: Operation
    let pointerInterval: Int
    let param1: Int?
    let param2: Int?
    let param3: Int?
    
    init(input1: Int, input2: Int, output: Int) {
        self.opcode = .multiply
        self.pointerInterval = 4
        self.param1 = input1
        self.param2 = input2
        self.param3 = output
    }
}

public struct HaltInstruction: Instruction {
    let opcode: Operation
    let pointerInterval: Int
    let param1: Int? = nil
    let param2: Int? = nil
    let param3: Int? = nil
    
    init() {
        self.opcode = .halt
        self.pointerInterval = 0
    }
}

public struct IntCode: Computer {
    var memory: [Int]
    public init(input: String = "") {
        var output = [Int]()
        for address in input.components(separatedBy: ",") {
            if let addressValue = Int(address) {
                output.append(addressValue)
            }
        }
        memory = output
    }

    private func createInstruction(index: Int = 0) -> Instruction {
        let opcode = memory[index]
        switch opcode {
        case 99:
            return HaltInstruction.init()
        case 1:
            return AddInstruction.init(input1: memory[index+1],
                                       input2: memory[index+2],
                                       output: memory[index+3])
            
        case 2:
            return MultiplyInstruction.init(input1: memory[index+1],
                                            input2: memory[index+2],
                                            output: memory[index+3])
        default:
            print("Unable to determine valid instruction type.")
            return HaltInstruction.init()
        }
    }
    
    mutating func runProgram() -> Int {
        var instructionPointer = 0
        var haltReached = false
        
        programExec: repeat {
            let instruction = createInstruction(index: instructionPointer)
            switch instruction.opcode {
            case .add:
                if let output = instruction.param3,
                    let input1 = instruction.param1,
                    let input2 = instruction.param2 {
                    memory[output] = memory[input1] + memory[input2]
                }
            case .multiply:
                if let output = instruction.param3,
                    let input1 = instruction.param1,
                    let input2 = instruction.param2 {
                    memory[output] = memory[input1] * memory[input2]
                }
            case .halt:
                haltReached = true
                break programExec
            }
            
            instructionPointer += instruction.pointerInterval
        } while !haltReached
        
        return memory[0]
    }
    
    mutating func setProgramInputs(noun: Int, verb: Int) {
        self.memory.replaceSubrange(1...2, with: [noun, verb])
    }
}

extension IntCode: Solution {
    public static func runSolution(path: String) -> String {
        if let streamReader = StreamReader(path: path),
            let initialInput = streamReader.nextLine() {
            streamReader.close()
            var computer: Computer = IntCode(input: initialInput)
            
            computer.setProgramInputs(noun: 12, verb: 2)
            
            return "Gravity assist program output: \(computer.runProgram())"
        }
        
        return "Could not run Int Code Computer Program."
    }
}
