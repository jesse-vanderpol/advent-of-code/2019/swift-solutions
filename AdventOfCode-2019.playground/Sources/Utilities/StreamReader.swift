import UIKit
import Foundation

class StreamReader {
    
    let encoding: String.Encoding
    let chunkSize: Int
    let delimData: Data
    var fileHandle: FileHandle!
    var buffer: Data
    var atEndOf: Bool
    
    init?(path: String,
          delimiter: String = "\n",
          encoding: String.Encoding = .utf8,
          chunkSize: Int = 4096) {
        guard let fileHandler = FileHandle(forReadingAtPath: path),
            let delimData = delimiter.data(using: encoding) else {
                return nil
        }
        
        self.encoding = encoding
        self.chunkSize = chunkSize
        self.fileHandle = fileHandler
        self.delimData = delimData
        self.buffer = Data(capacity: chunkSize)
        self.atEndOf = false
    }
    
    deinit {
        self.close()
    }
    
    func nextLine() -> String? {
        precondition(fileHandle != nil, "Attempted to read from a closed file")
        
        while !atEndOf {
            if let range = buffer.range(of: delimData) {
                let line = String(data: buffer.subdata(in: 0..<range.lowerBound), encoding: encoding)
                
                buffer.removeSubrange(0..<range.upperBound)
                return line
            }
            let tmpData = fileHandle.readData(ofLength: chunkSize)
            if tmpData.count > 0 {
                buffer.append(tmpData)
            } else {
                atEndOf = true
                if buffer.count > 0 {
                    let line = String(data: buffer as Data, encoding: encoding)
                    buffer.count = 0
                    return line
                    
                }
            }
        }
        return nil
    }
    
    func rewind() -> Void {
        fileHandle.seek(toFileOffset: 0)
        buffer.count = 0
        atEndOf = false
    }
    
    func close() -> Void {
        fileHandle?.closeFile()
        fileHandle = nil
    }
}

extension StreamReader : Sequence {
    func makeIterator() -> AnyIterator<String> {
        return AnyIterator {
            return self.nextLine()
        }
    }
}
