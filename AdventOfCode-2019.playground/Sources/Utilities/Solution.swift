public protocol Solution {
    static func runSolution(path: String) -> String
}
