import UIKit

protocol RocketFuelCalculator {
    func calculateFuelNeeded(mass: Int) -> Int
}

public struct SantaSleighFuelRequirements: RocketFuelCalculator {
    public init() {}
    
    func fuelForMass(mass: Int) -> Int {
        return mass / 3 - 2
    }
    
    func calculateFuelForFuel(mass: Int) -> Int {
        var fuelAccumulator: Int = fuelForMass(mass: mass)
        if fuelAccumulator < 0 {
            return 0
        }
        
        fuelAccumulator += calculateFuelForFuel(mass: fuelAccumulator)
        return fuelAccumulator
    }
    
   func calculateFuelNeeded(mass: Int) -> Int {
        let fuelNeededForModule = fuelForMass(mass: mass)
        let fuelNeededForFuel = calculateFuelForFuel(mass: fuelNeededForModule)
        
        return fuelNeededForModule + fuelNeededForFuel
    }
}

extension SantaSleighFuelRequirements: Solution {
    public static func runSolution(path: String) -> String {
        if let streamReader = StreamReader(path: path) {
            
            var totalFuelRequirement: Int = 0
            defer {
                streamReader.close()
            }
            while let line = streamReader.nextLine() {
                if let mass = Int(line) {
                    let calculator: RocketFuelCalculator = SantaSleighFuelRequirements()
                    totalFuelRequirement += calculator.calculateFuelNeeded(mass: mass)
                }
            }
            return "The Total Module Fuel Requirement: \(totalFuelRequirement)"
        }
        return "Could Not File Module Fuel Requirement."
    }
}
